<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page import="java.util.Map"%>
<!DOCTYPE html>
<html lang="es">
<head>
    <title>Votaci&oacute;n mejor jugador liga ACB</title>
    <link href="estilos.css" rel="stylesheet" type="text/css" />
</head>
<body class="resultado">
        
        <table border="2">
            <tr>
              <td>Jugador</td><td>Votos</td>
            </tr>
            <%
            Map<String,Integer> jugadores = ((Map<String,Integer>) session.getAttribute("jugadores"));
               
              for(Map.Entry<String, Integer> entry : jugadores.entrySet()) {
                out.println("<tr>");
                out.println("<td>" +  entry.getKey() + "</td>");
                out.println("<td>" +  entry.getValue() + "</td>");
                out.println("</tr>");
              }
            %>    
          </table>
    
        
      <br> <a href="index.html"> Ir al comienzo</a>
</body>
</html>
