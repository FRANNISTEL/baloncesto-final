import static org.junit.jupiter.api.Assertions.assertEquals;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;


import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

public class ModeloDatosTest {

	private static String JDBC_DRIVER = "org.h2.Driver";
	private static String DB_URL = "jdbc:h2:~/test2";
	private static String USER = "sa";
	private static String PASS = "";
	private static ModeloDatos modeloDatos= new ModeloDatos();
	private static Connection conn;
    private static Statement stmt = null; 

	@BeforeAll
	public static void setUp() throws ClassNotFoundException, SQLException {
        Class.forName(JDBC_DRIVER); 
        conn = DriverManager.getConnection(DB_URL,USER,PASS);
        stmt = conn.createStatement();
        stmt.execute("Drop TABLE Jugadores");
        stmt.execute("CREATE TABLE Jugadores (votos INTEGER not NULL,  nombre VARCHAR(255), PRIMARY KEY ( nombre ) )");
		stmt.executeUpdate("INSERT INTO Jugadores " + " (nombre,votos) VALUES ('" + "test" + "',1)");
        modeloDatos.setCon(conn);        
	}
	
	@AfterAll
	public static void cleanUp() throws SQLException{
		stmt.execute("Drop TABLE Jugadores");
	}

  
	@Test
	public void testExisteJugador() {
		System.out.println("Prueba de existeJugador");
		String nombre = "";
		ModeloDatos instance = new ModeloDatos();
		boolean expResult = false;
		boolean result = instance.existeJugador(nombre);
		assertEquals(expResult, result);

	}
	
	@Test
	public void testActualizarJugador() throws SQLException {
  	System.out.println("Test actualizar jugador.");
		int nVotos;
		modeloDatos.actualizarJugador("test");		
		ResultSet rs=stmt.executeQuery("Select votos from Jugadores WHERE nombre='test'");
		rs.next();
		nVotos = rs.getInt("votos");
		assertEquals(nVotos,2);
	}
	
	

}