<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html lang="es">
<head>
    <title>Votaci&oacute;n mejor jugador liga ACB</title>
    <link href="estilos.css" rel="stylesheet" type="text/css" />
</head>
<body class="resultado">
    <%
    <c:forEach var="entry" items="${jugadoresVotos}">
        key is ${entry.key}
        <c:forEach var="info" items="${entry.value}">
             info is ${info}
        </c:forEach>
     </c:forEach>
     %>
    
        
        <br> <a href="index.html"> Ir al comienzo</a>
</body>
</html>
